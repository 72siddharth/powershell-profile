# Access Sysinfo
$TimeHour = (get-date).hour
$UserName = get-content env:\UserName

#Welcome Message
if($TimeHour -ge 16) {
	write-host "  Welcome $UserName" -foregroundcolor DarkMagenta
} else {
	write-host "  Welcome $UserName" -foregroundcolor DarkYellow
}
echo ''

# Command Aliases
Set-Alias vim nvim

# Invoke Starship Prompt
Invoke-Expression (&starship init powershell)
# Set Starship Logs Directory
$ENV:STARSHIP_CACHE = "$HOME\AppData\Local\temp"

# Local Path Variables
$ALACRITTY_CONFIG = "$HOME\AppData\Roaming\alacritty\alacritty.yml"
$STARSHIP_CONFIG = "$HOME\.config\starship.toml"
$VIMRC = "C:\Users\hp\AppData\Local\nvim\init.vim"
